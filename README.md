# Nitter Android (Web)App

## About
This is a client for the popular Twitter front-end, [Nitter](https://github.com/zedeus/nitter). <br>
Thanks to Android WebView, you can have a completely separate app for Nitter and isolate your cookies and history from your main browser app, which is a privacy bonus.

**DISCLAIMER:** This app is only a client, therefore I cannot help you if the issue you are experiencing is with Nitter itself. Any issues directing towards the Nitter service should be redirected [upstream on GitHub.](https://github.com/zedeus/nitter/issues)

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.plexer0.nitter/)

or get the APK from the [Releases section](https://gitlab.com/Plexer0/Nitter-Android/-/releases).

## Nitter Instance
Out of the box, [nitter.net](https://nitter.net) is the default instance the app will use. If this is undesired, you can set the preferred instance within the app on Android 7+. <br>
You can access these preferences on the App Info page and tapping on the cog icon. <br>

## JavaScript
For the sake of convenience, JavaScript is enabled by default so you can run hls video content out of the box.
If you wish to disable JavaScript completely, open the App Info page, tap on the cog icon, and toggle the JavaScript switch on and off.

## Contributions
**The following people have contributed to this application. Special thanks for the support!** <br>
* [Poussinou](https://gitlab.com/Poussinou) - !1 <br>
* [vyneer](https://gitlab.com/vyneer) - !2 <br>

If you would like to help out, see [CONTRIBUTING.md](CONTRIBUTING.md).
