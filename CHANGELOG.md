# Changelog
**NOTE:** This is a stub. The real changelogs can be found at the following locations:

Fastlane: [fastlane/metadata/android/en-US/changelogs](fastlane/metadata/android/en-US/changelogs)  
GitLab: [/-/releases](https://gitlab.com/Plexer0/Nitter-Android/-/releases)  
Git Tags: [/-/tags](https://gitlab.com/Plexer0/Nitter-Android/-/tags)
