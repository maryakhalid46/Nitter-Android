What's new and improved:

* [FIX] Fixed images from loading at original size
* [CHR] Reflected changes to upstream's instance list that will launch in the application
